#include "central_quarantine.h"
#include "helper_defs.h"
#include "malloc.h"
#include "memory_ranges.h"
#include "proc_maps_parse.h"
#include <pthread.h>
#include "quarantine.h"
#include <thread>

void quarantine_insert(void *ptr, size_t alloc_size) {
    local_quarantine.insert(ptr, alloc_size);
}

void Quarantine::insert(void *ptr, size_t size) {
    Item item(ptr, size);

    items().push_back(item);

    // Only signal other thread after passing NEWLY_INSERTED_THRESHOLD bytes
    if ((newly_inserted += size) > minesweeper::NEWLY_INSERTED_THRESHOLD) {
        local_stackMemRanges.refreshStackTop();

        central_quarantine.moveAllFrom(items());
        newly_inserted = 0;
    }
}

thread_local Quarantine local_quarantine;

