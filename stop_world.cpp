#include "central_quarantine.h"
#include "helper_defs.h"
#include "minesweeper_config.h"
#include "stop_world.h"

StopTheWorld& stop_the_world() {
    static StopTheWorld stw;
    return stw;
}

NOINLINE static void save_caller_regs() { asm(""); }

extern "C" {
static void suspend_handler(int sig, siginfo_t *si, void *raw_sc) {
//  outputRaw("suspend_handler()\n");
  stop_the_world().sigsus();
}

static void resume_handler(int sig, siginfo_t *si, void *raw_sc) {
//  outputRaw("resume_handler()\n");
  // Do nothing, resumes suspend_handler on sigsuspend
}

void add_thread(pthread_t thd) { stop_the_world().addThread(thd); }
};

StopTheWorld::StopTheWorld() {
  ASSERT(!sigfillset(&suspend_wait_mask));
  ASSERT(!sigdelset(&suspend_wait_mask, SIG_RESUME_WORLD));
}

void StopTheWorld::resetThreads() {
  msweeper_dlog("resetThreads()\n");
  threadCount.store(0, std::memory_order_release);
}

void StopTheWorld::addThread(pthread_t thd) {
  msweeper_dlog("addThread()\n");

  { WITH_LOCK(threadsLock);
    threads[threadCount.load(std::memory_order_relaxed)] = thd;
    threadCount.fetch_add(1, std::memory_order_release);
  }

  // Set up handlers
  struct sigaction act, oldact;
  act.sa_flags = SA_RESTART | SA_SIGINFO;

  ASSERT(!sigfillset(&act.sa_mask));
  ASSERT(!sigdelset(&act.sa_mask, SIGINT));
  ASSERT(!sigdelset(&act.sa_mask, SIGQUIT));
  ASSERT(!sigdelset(&act.sa_mask, SIGABRT));
  ASSERT(!sigdelset(&act.sa_mask, SIGTERM));

  act.sa_sigaction = suspend_handler;
  ASSERT(!sigaction(SIG_STOP_WORLD, &act, &oldact));

  act.sa_sigaction = resume_handler;
  ASSERT(!sigaction(SIG_RESUME_WORLD, &act, &oldact));
}

inline void StopTheWorld::sigsus() {
  ucontext_t ctx;
  ASSERT(!getcontext(&ctx));
  save_caller_regs();

  uint64_t sc = StopCount.load(std::memory_order_acquire);
  ASSERT(sc % 2);

  do {
    StoppedThreads.fetch_add(1, std::memory_order_release);
    sigsuspend(&suspend_wait_mask);
    ASSERT(errno == EINTR);
    sc = StopCount.load(std::memory_order_acquire);
  } while (sc % 2);
}

void StopTheWorld::protectHeap() {
  msweeper_dlog("protectHeap()\n");
  int fd = open("/proc/self/clear_refs", O_WRONLY);
  if (fd < 0) printf("%d\n", errno);
  ASSERT(fd >= 0);
  write(fd, (const void*)"4", 1);
  close(fd);
}


void StopTheWorld::stop() {
  msweeper_dlog("stop()\n");
  pagemapFd = open("/proc/self/pagemap", O_RDONLY);
  pagemapOff = 0;
  ASSERT(pagemapFd >= 0);

  // Acquire mutexes for iteration. Do it now, as doing so after
  // suspending other threads could lead to deadlock.
  memory_ranges().lock();

  uint32_t tc = threadCount.load(std::memory_order_acquire);

  StopCount.fetch_add(1, std::memory_order_release);
  for (uint32_t i = 0; i < tc; i++)
    ASSERT(!pthread_kill(threads[i], SIG_STOP_WORLD));

  while (StoppedThreads.load(std::memory_order_acquire) < tc)
    pthread_yield();
}

thread_local pagemap_buff_t pmapLocalBuffer;

void StopTheWorld::invalidateBuffer() { pmapLocalBuffer.invalidate(); }

bool StopTheWorld::isPageDirty(uintptr_t page) {
  page /= PAGE_SIZE;
  bool result = 0;

  if (pmapLocalBuffer.read(page, result))
    return result;

  off_t offset = page * 8; // 8 Bytes per page in map

  if (offset != pagemapOff) {
    pagemapOff = lseek(pagemapFd, offset, SEEK_SET);
    ASSERT(pagemapOff== offset);
  }
  pmapLocalBuffer.size = read(pagemapFd, (void*)pmapLocalBuffer.buff,
                              PAGEMAP_BUFF_SIZE*8) / 8;
  pmapLocalBuffer.beg = page;

  ASSERT(pmapLocalBuffer.read(page, result));
  return result;
}

void StopTheWorld::resume() {
  msweeper_dlog("resume()\n");
  memory_ranges().unlock();

  uint32_t tc = threadCount.load(std::memory_order_acquire);

  StopCount.fetch_add(1, std::memory_order_relaxed);
  for (uint32_t i = 0; i < tc; i++)
    ASSERT(!pthread_kill(threads[i], SIG_RESUME_WORLD));

  StoppedThreads.store(0, std::memory_order_relaxed);
  close(pagemapFd);
}

