// This file contains code for parsing lines of /proc/self/maps, which contains the memory map of the process.

#include "memory_ranges.h"
#include "stddef.h"
#include "stdint.h"

struct ProcRegion {
    void* start_ptr;
    void* end_ptr;
    bool readable, writable, executable, CoW;
    size_t offset;
    // const char *dev;
    uintptr_t inode;
    // const char *pathname;
    bool has_path;
    bool from_jemalloc;
    bool stack;
    bool heap;

    // Construct object by parsing a line of /proc/self/maps passed as a char buffer
    ProcRegion(char *line);
};

