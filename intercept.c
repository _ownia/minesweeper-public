// RTLD_NEXT and dlsym to get the original free() function
#ifndef _GNU_SOURCE
    #define _GNU_SOURCE
#endif
#include "dlfcn.h"

#include "jemalloc/jemalloc.h"
#include "malloc.h"
#include "quarantine.h"
#include "stop_world.h"
#include "string.h"

#define MALLOC_LIB "libjemalloc.so"
#define SWEEPER_LIB "libminesweeper.so"
#ifdef LIB_DIR
    #define MALLOC_PATH (LIB_DIR "/" MALLOC_LIB)
    #define SWEEPER_PATH (LIB_DIR "/" SWEEPER_LIB)
#else 
    #define MALLOC_PATH MALLOC_LIB
    #define SWEEPER_PATH SWEEPER_LIB
#endif

void (*real_free)(void*);
void* (*je_rallocx)(void*, size_t, int);
void (*je_sdallocx)(void*, size_t, int);

typedef int (*pthread_create_t)(pthread_t *__restrict,
                                const pthread_attr_t *,
                                void *(*)(void *),
                                void *__restrict);

static pthread_create_t real_pthread_create;

void free(void *ptr) noexcept {
    if (!ptr) return;
    quarantine_insert(ptr, sallocx(ptr, 0));
}

inline void free(void *ptr, size_t size) {
    if (!ptr) return;
    quarantine_insert(ptr, size);
}

void *realloc(void *ptr, size_t new_size) noexcept {
    if (!ptr) return malloc(new_size);

    size_t nomove_size = sallocx(ptr, 0);
    if (nomove_size >= new_size)
        return ptr;

    void *new_ptr = malloc(new_size);
    ASSERT(new_ptr);

    memmove(new_ptr, ptr, nomove_size);
    quarantine_insert(ptr, nomove_size);
    return new_ptr;
}

void *rallocx(void *ptr, size_t new_size, int flags) {
    size_t nomove_size = sallocx(ptr, flags);
    if (nomove_size >= new_size) {
        void *new_ptr = je_rallocx(ptr, new_size, flags);
        ASSERT(new_ptr == ptr);
        return ptr;
    }

    void *new_ptr = mallocx(new_size, flags);
    ASSERT(new_ptr);

    memmove(new_ptr, ptr, nomove_size);
    quarantine_insert(ptr, nomove_size);
    return new_ptr;
}

void dallocx(void *ptr, int flags) {
    quarantine_insert(ptr, sallocx(ptr, flags));
    // TODO store flags for freeing with dallocx later (instead of using free()...)
}

void sdallocx(void *ptr, size_t size, int flags) {
    quarantine_insert(ptr, size);
}

void operator delete[](void *ptr) noexcept { free(ptr); }
void operator delete[](void *ptr, const std::nothrow_t &) noexcept { free(ptr); }
void operator delete[](void *ptr, size_t size) noexcept { free(ptr, size); }
void operator delete(void *ptr) noexcept { free(ptr); }
void operator delete(void *ptr, const std::nothrow_t &) noexcept { free(ptr); }
void operator delete(void *ptr, size_t size) noexcept { free(ptr, size); }

int pthread_create(pthread_t *__restrict thread, const pthread_attr_t *attr,
                   void *(*start_routine)(void *), void *__restrict arg) {
    if (!real_pthread_create)
        real_pthread_create = (pthread_create_t)dlsym(RTLD_NEXT, "pthread_create");

    int result = real_pthread_create(thread, attr, start_routine, arg);

    if (minesweeper::STOP_WORLD_RESWEEP && !result)
        add_thread(*thread);

    return result;
}

void _init_intercept() {
    void *malloc_lib = dlopen(MALLOC_PATH, RTLD_NOW | RTLD_GLOBAL);
    ASSERT(malloc_lib);
    real_free = (void(*)(void*))dlsym(malloc_lib, "free");
    je_rallocx = (void*(*)(void*,size_t,int))dlsym(malloc_lib, "rallocx");
    je_sdallocx = (void(*)(void*,size_t,int))dlsym(malloc_lib, "sdallocx");

    // Force symbol resolution now, as it causes libdl to call malloc, which
    // causes issues later (deadlock, reentrancy bugs)...
    void *sweeper_lib = dlopen(SWEEPER_PATH, RTLD_NOW | RTLD_NOLOAD);
    ASSERT(sweeper_lib);
}

