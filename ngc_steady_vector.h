// Non-garbage-collected vector which guarantees no relocation of underlying container
//      (i.e. pointers to elements are never invalidated)
#ifndef __NGC_STEADY_VECTOR_H_INCLUDED_
#define __NGC_STEADY_VECTOR_H_INCLUDED_

#include <cstddef>
#include "helper_defs.h"
#include "ngc_vector.h"
#include "sys/mman.h"
#include <utility>

#define BLOCK_SIZE 16*4096

template <class T>
class ngc_steady_vector {
    ngc_vector<T*> pages;
    size_t _size;

    inline void grow();
    inline void growForOneMoreElement();

    const size_t elements_per_page = BLOCK_SIZE / sizeof(T);
    inline size_t page_index_of(size_t index) const { return index / elements_per_page; }
    inline size_t offset_of(size_t index) const { return index % elements_per_page; }

    inline T* page_of(size_t index) const { return pages[page_index_of(index)]; }
  public:
    ngc_steady_vector();

    inline T& operator[](const size_t i) const { return page_of(i)[offset_of(i)]; }

    inline size_t size() { return _size; }
    inline bool empty() { return !_size; }

    void push_back(const T& item);
    template<typename... Args> void emplace_back(Args&&... args);
    void pop_back() { ASSERT(_size); if (_size) _size--; ~((*this)[_size])();}

    T &first() const { ASSERT(_size); return pages[0][0]; }
    T &last() const { ASSERT(_size); return (*this)[_size-1]; }
};

/** Implementation. In this header, so that the template is instantiated correctly. */
template <class T>
void ngc_steady_vector<T>::grow() {
    T* new_page =
            (T*)mmap(NULL, BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE | MAP_POPULATE, -1, 0);
    ASSERT(new_page != MAP_FAILED);

    pages.push_back(new_page);
}

template <class T>
void ngc_steady_vector<T>::growForOneMoreElement() {
    if (page_index_of(_size) == pages.size()) grow();
}

template <class T>
void ngc_steady_vector<T>::push_back(const T& item) {
    growForOneMoreElement();
    (*this)[_size++] = item;
}

template <class T>
template <typename... Args>
void ngc_steady_vector<T>::emplace_back(Args&&... args) {
    growForOneMoreElement();
    new (&((*this)[_size++])) T(std::forward<Args>(args)...);
}

template <class T>
ngc_steady_vector<T>::ngc_steady_vector() {
    ASSERT(sizeof(T) < BLOCK_SIZE);
    _size = 0;
}

#endif //__NGC_STEADY_VECTOR_H_INCLUDED_

