#ifndef __SHADOW_H_INCLUDED_
#define __SHADOW_H_INCLUDED_

#include "stdint.h"
#include "stddef.h"
#include "sys/mman.h"

#define PAGE_SIZE 4096
#define MAX_ADDR (((size_t)1) << 47)

// Shadow structure. Logically, this is a flat array of bits, one per word of memory.

template<int GRANULE_BYTES>
class ShadowMap {
  public:
    using T = uintmax_t;
    using mapT = std::atomic<T>;
  private:
    static constexpr size_t marks_per_entry = sizeof(T) * 8;
    static constexpr size_t shadow_size = (MAX_ADDR / GRANULE_BYTES) / marks_per_entry;
    static constexpr size_t map_size = shadow_size * sizeof(mapT);
    // On-demand bitmaps. These will be mapped to cover the entire address space,
    // but only backed by physical pages if (part of) the corresponding region is.
    mapT* marks; // Was there a pointer pointing to this word?

    inline uintptr_t ptr2granule(void *ptr) { return ((uintptr_t)ptr) / GRANULE_BYTES; }

    inline T mark(mapT* mark_word, T mask)
        { return std::atomic_fetch_or_explicit(mark_word, mask, std::memory_order_relaxed) & mask; }
    inline void unmark(mapT* mark_word, T mask)
        { std::atomic_fetch_and_explicit(mark_word, ~mask, std::memory_order_relaxed); }
    inline bool is_marked(mapT* mark_word, T mask)
        { return std::atomic_load_explicit(mark_word, std::memory_order_relaxed) & mask; }

  public:
    ShadowMap();
    void clear(); // Call after each scan
    void clear(void *start, void *end);
    void clearApprox(void *range_start, void *range_end);
    bool marked(void* ptr, size_t len);
    inline bool marked(void* ptr) { return is_marked(entry(ptr), mask(ptr)); }
    inline bool mark(void* ptr);
    void mark(void* start, void* end);

    inline mapT* entry(void* ptr) { return &marks[ptr2granule(ptr) / marks_per_entry]; }
    inline T mask(void* ptr) { return ((T)1) << (ptr2granule(ptr) % marks_per_entry); }
};

template<int GRANULE_BYTES>
inline bool ShadowMap<GRANULE_BYTES>::mark(void* ptr) { return (bool)mark(entry(ptr), mask(ptr)); }

template<int GRANULE_BYTES>
ShadowMap<GRANULE_BYTES>::ShadowMap() {
    ASSERT(sizeof(T) == sizeof(mapT));
    ASSERT(mapT::is_always_lock_free);

    marks = (mapT*)mmap(NULL,
                            map_size,
                            PROT_WRITE | PROT_READ,
                            MAP_PRIVATE | MAP_ANON | MAP_NORESERVE,
                            -1, 0);
    madvise((void*)marks, map_size, MADV_NOHUGEPAGE);
    ASSERT(marks != MAP_FAILED);
}

template<int GRANULE_BYTES>
void ShadowMap<GRANULE_BYTES>::clearApprox(void *range_start, void *range_end) {
    void *addr = page_aligned((void*)entry(range_start));
    void *end_addr = page_aligned_up((void*)(entry(range_end)+1));
    size_t size = size_bytes(addr, end_addr);

    ASSERT(mmap(addr, size,
                PROT_WRITE | PROT_READ,
                MAP_PRIVATE | MAP_ANON | MAP_NORESERVE | MAP_FIXED,
                -1, 0) == addr);
}

template<int GRANULE_BYTES>
void ShadowMap<GRANULE_BYTES>::clear(void *start, void *end) {
    T startMask = ~(mask(start)-1);
    T endMask = mask(end)-1;

    if (entry(start) == entry(end)) {
        unmark(entry(start), startMask & endMask);
    } else {
        unmark(entry(start), startMask);
        unmark(entry(end), endMask);
        for (mapT* t = entry(start) + 1; t < entry(end); t++)
            unmark(t, (T)-1);
    }
}

template<int GRANULE_BYTES>
void ShadowMap<GRANULE_BYTES>::mark(void *start, void *end) {
    T startMask = ~(mask(start)-1);
    T endMask = mask(end)-1;

    if (entry(start) == entry(end)) {
        mark(entry(start), startMask & endMask);
    } else {
        mark(entry(start), startMask);
        mark(entry(end), endMask);
        for (mapT* t = entry(start) + 1; t < entry(end); t++)
            mark(t, (T)-1);
    }
}

template<int GRANULE_BYTES>
bool ShadowMap<GRANULE_BYTES>::marked(void *start, size_t len) {
    void *end = add_bytes(start, len);
    T startMask = ~(mask(start)-1);
    T endMask = mask(end)-1;

    if (entry(start) == entry(end)) {
        return is_marked(entry(start), startMask & endMask);
    } else {
        if (is_marked(entry(start), startMask) || is_marked(entry(end), endMask))
            return true;
        for (mapT* t = entry(start) + 1; t < entry(end); t++)
            if (is_marked(t, (T)-1)) return true;
        return false;
    }
}

template<int GRANULE_BYTES>
void ShadowMap<GRANULE_BYTES>::clear() {
    void* ret = mmap((void*)marks,
                     map_size,
                     PROT_WRITE | PROT_READ,
                     MAP_PRIVATE | MAP_ANON | MAP_NORESERVE | MAP_FIXED,
                     -1, 0);
    ASSERT(ret == (void*)marks);
    madvise((void*)marks, map_size, MADV_NOHUGEPAGE);
}

template<int GRANULE_BYTES> static constexpr size_t marks_per_entry;
template<int GRANULE_BYTES> static constexpr size_t shadow_size;
template<int GRANULE_BYTES> static constexpr size_t map_size;

using ShadowSpace = ShadowMap<8>;
using PageShadow = ShadowMap<PAGE_SIZE>;

#endif //__SHADOW_H_INCLUDED

