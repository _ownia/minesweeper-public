#ifndef __NGC_VECTOR_H_INCLUDED_
#define __NGC_VECTOR_H_INCLUDED_

#include <cstddef>
#include "sys/mman.h"
#include <utility>

template <class T>
class ngc_vector {
    size_t BLOCK_SIZE;

    T* arr;
    size_t _size, capacity;

    inline void grow();
  public:
    ngc_vector(size_t size = 1);

    inline T& operator[](const size_t i) const { return arr[i]; }

    inline size_t size() const { return _size; }
    inline bool empty() const { return !_size; }

    void push_back(const T& item);
    template<typename... Args> void emplace_back(Args&&... args);
    void pop_back() { ASSERT(_size); if (_size) _size--; }
    void pop_back(size_t x) { ASSERT(_size >= x); ASSERT(x >= 0); _size -= x; }
    void add_all(const ngc_vector<T>& other);
    inline void erase_all() { _size = 0; }

    T &first() { ASSERT(_size); return arr[0]; }
    T &last() { ASSERT(_size); return arr[_size-1]; }

    using iterator = T*;

    inline iterator begin() const { return arr; }
    inline iterator end() const { return arr+_size; }

    void resize(size_t new_capacity);
    inline void reserve(size_t new_capacity);
};

/** Implementation. In this header, so that the template is instantiated correctly. */
template <class T>
void ngc_vector<T>::resize(size_t new_capacity) {
    ASSERT((new_capacity > size()) || (!new_capacity && empty()));

    size_t old_size = capacity * sizeof(T);

    if (!new_capacity) {
        munmap(arr, old_size);
        arr = NULL;
        capacity = new_capacity;
    } else {
        if ((new_capacity < capacity) || (!capacity))
            capacity = BLOCK_SIZE / sizeof(T); // Use one page to start with
        while (capacity < new_capacity) capacity *= 2;
        size_t new_size = capacity * sizeof(T);

        if (old_size == new_size) return; // Nothing to do

        if (old_size) {
            arr = (T*)mremap(arr, old_size, new_size, MREMAP_MAYMOVE);
        } else {
            arr = (T*)mmap(arr, new_size, PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0);
        }
    }
    ASSERT(arr != MAP_FAILED);
}

template <class T>
inline void ngc_vector<T>::reserve(size_t new_capacity) {
    if (new_capacity <= capacity) return;
    resize(new_capacity);
}

template <class T>
inline void ngc_vector<T>::grow() {
    reserve(_size + 1);
}

template <class T>
void ngc_vector<T>::push_back(const T& item) {
    arr[_size++] = item;
    grow();
}

template <class T>
template <typename... Args>
void ngc_vector<T>::emplace_back(Args&&... args) {
    new (&arr[_size]) T(std::forward<Args>(args)...);
    _size++;
    grow();
}

template <class T>
void ngc_vector<T>::add_all(const ngc_vector<T>& other) {
    reserve(size() + other.size() + 1);
    for (const T& t : other) arr[_size++] = t;
}

template <class T>
ngc_vector<T>::ngc_vector(size_t size) {
    BLOCK_SIZE = size * 4096;
    ASSERT(sizeof(T) < BLOCK_SIZE);
    _size = 0;
    capacity = BLOCK_SIZE / sizeof(T); // Use 8 pages to start with
    arr = (T*)mmap(NULL, BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE | MAP_POPULATE, -1, 0);
    ASSERT(arr != MAP_FAILED); // TODO error handling
}

#endif //__NGC_VECTOR_H_INCLUDED_

