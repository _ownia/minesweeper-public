#include "helper_defs.h"
#include "jemalloc/jemalloc.h"                  // mallctl()
#include "memory_ranges.h"
#include "malloc.h"

#define EHOOKS_SUCCESS false
#define EHOOKS_FAILURE true

static extent_hooks_t *original_extent_hooks;
static extent_hooks_t new_extent_hooks;
static extent_hooks_t *new_extent_hooks_ptr;

bool extent_commit(
    extent_hooks_t *extent_hooks,
    void *addr,
    size_t size,
    size_t offset,
    size_t length,
    unsigned arena_ind)
{
    remap_mem_range(addr, size);
    return EHOOKS_SUCCESS;
}

bool extent_decommit(
    extent_hooks_t *extent_hooks,
    void *addr,
    size_t size,
    size_t offset,
    size_t length,
    unsigned arena_ind)
{
    unmap_mem_range(addr, size);
    return EHOOKS_SUCCESS;
}

static int mallctl_i(const char* str, int i, void *oldv,
                     size_t *old_size, void *newv, size_t new_size) {
    char mallctl_str[100];
    sprintf(mallctl_str, str, i);

    return (mallctl(mallctl_str, oldv, old_size, newv, new_size));
}

static size_t purge_mib[3];
static size_t purge_mib_len;

static size_t tcache_flush_mib[3];
static size_t tcache_flush_mib_len;

void _init_malloc() {
    if (original_extent_hooks)
      return;

    size_t old_size; // Used with mallctl

    // Get the number of arenas
    int narenas;
    old_size = sizeof(int);
    ASSERT(!mallctl("arenas.narenas", &narenas, &old_size, NULL, 0));

    // Read and save original hooks
    old_size = sizeof(void*);
    ASSERT(!mallctl("arena.0.extent_hooks", &original_extent_hooks, &old_size, NULL, 0));

    // Set up new hooks
    new_extent_hooks.alloc = original_extent_hooks->alloc;
    new_extent_hooks.alloc_base = original_extent_hooks->alloc_base;
    new_extent_hooks.dalloc = NULL;
    new_extent_hooks.destroy = NULL;
    new_extent_hooks.commit = extent_commit;
    new_extent_hooks.decommit = extent_decommit;
    new_extent_hooks.purge_lazy = NULL;
    new_extent_hooks.purge_forced = NULL;
    new_extent_hooks.split = original_extent_hooks->split;
    new_extent_hooks.merge = original_extent_hooks->merge;
    new_extent_hooks_ptr = &new_extent_hooks;

    // Get threads default arena
    unsigned thread_arena;
    old_size=sizeof(thread_arena);
    int ret = mallctl("thread.arena", &thread_arena, &old_size, NULL, 0);
    ASSERT(!ret);

    // For each arena
    for (int i=0; i<narenas; i++) {
        // Change alloc and dealloc hooks
        bool is_initialised;
        old_size=sizeof(is_initialised);
        mallctl_i("arena.%d.initialized", i, &is_initialised, &old_size, NULL, 0);

        if (!is_initialised) {
            // HACK: Trigger initialisation without crashing
            unsigned arena = i;
            ret = mallctl_i("thread.arena", i, NULL, NULL, &arena, sizeof(arena));
            ASSERT(!ret);
        }

        ret = mallctl_i("arena.%d.extent_hooks", i, NULL, NULL,
                            &new_extent_hooks_ptr, sizeof(new_extent_hooks_ptr));
        ASSERT(!ret);
    }

    // Reset thread's default arena in case we changed it (see HACK above)
    ASSERT(!mallctl("thread.arena", NULL, 0, &thread_arena, sizeof(thread_arena)));

    purge_mib_len = 3;
    ASSERT(!mallctlnametomib("arena.0.purge", purge_mib, &purge_mib_len));
    purge_mib[1] = MALLCTL_ARENAS_ALL;

    tcache_flush_mib_len = 3;
    ASSERT(!mallctlnametomib("thread.tcache.flush", tcache_flush_mib, &tcache_flush_mib_len));
}


void purge_all() {
    ASSERT(!mallctlbymib(purge_mib, purge_mib_len, NULL, NULL, NULL, 0));
}

void flush_tcache() {
    ASSERT(!mallctlbymib(tcache_flush_mib, tcache_flush_mib_len, NULL, NULL, NULL, 0));
}

