#include "stddef.h"

#ifdef __cplusplus
    extern "C" {
#endif
void free(void *ptr) noexcept;
#ifdef __cplusplus
    }
#endif

extern void (*real_free)(void *ptr);
extern void (*je_sdallocx)(void *ptr, size_t size, int flag);

// Returns the total amount of memory allocated on the heap
size_t total_allocated_memory();

void _init_malloc();

// Free up all unused pages of memory in the allocator
void purge_all();
void flush_tcache();

