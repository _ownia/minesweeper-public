#include "helper_defs.h"
#include "malloc.h"
#include "memory_ranges.h"
#include "proc_maps_parse.h"
#include "stdint.h"
#include "stop_world.h"

MemoryRanges& memory_ranges() {
    static MemoryRanges memory_ranges;
    return memory_ranges;
}

void registerMapRanges() {
    FILE *maps = fopen("/proc/self/maps", "r");
    char buff[255];
    while (fgets(buff, sizeof(buff), maps) != NULL) {
        ProcRegion pr(buff);

        // Do not scan regions with special protection (unused or not containing heap ptrs)
        if (!pr.readable || !pr.writable || pr.executable) continue;

        if (pr.heap) {
            memory_ranges().heapPtr.ST_STORE(pr.start_ptr);
            continue;
        }

        // Heap regions should be handled via extent registration (managed) to avoid
        //     scanning jemalloc metadata, ngc regions and double-scanning
        if ((!pr.has_path) || (pr.heap) || (pr.stack)) continue;

        // Do not scan shared mappings to mmap'd files (should not contain pointers)
        // Note: .text .bss and .data are mapped as private file mappings
        if (!pr.CoW) continue;

        // Ignore large mappings (these likely just take advantage of overcommitting)
        if (size_bytes(pr.start_ptr, pr.end_ptr) > (1ll << 30)/*1GB*/) continue;

        // Ignore regions mapped from jemalloc. These may be for metadata that should be
        // conceptually non-garbage-collected.
        if (pr.from_jemalloc) continue;

        memory_ranges().addMapRange(pr.start_ptr, pr.end_ptr);
    }

    fclose(maps);
}

thread_local StackMemoryRange local_stackMemRanges;

void MemoryRanges::addMapRange(void *start, void *end) {
    ASSERT(page_aligned(start) == start);
    ASSERT(page_aligned(end) == end);

    mapRangeStarts[mapRangeCount] = start;
    mapRangeSizes[mapRangeCount++] = size_bytes(start, end);
}

int MemoryRanges::addStack(StackMemoryRange *stack) {
    WITH_LOCK(mtx)
    if (launching_sweeper) return StackMaxCount;

    for (int id = 0; id < StackMaxCount; id++) {
        if (stacks[id] != NULL) continue;

        if (id == stackCount) stackCount++;
        stacks[id] = stack;
        return id;
    }

    msweeper_log("Unable to register stack: already have MemoryRanges::MaxStackCount stacks alive.");
    ASSERT(false);
    return StackMaxCount;
}

void MemoryRanges::removeStack(int stack_id) {
    if (stack_id == StackMaxCount) return;
    WITH_LOCK(mtx)
    stacks[stack_id] = NULL;
    while ((stackCount > 0) && (stacks[stackCount-1] != NULL)) stackCount--;
}

void MemoryRanges::unmapRange(void *addr, size_t size) {
    unmappedPages.mark(addr, add_bytes(addr, size));
    unmappedMem.fetch_add(size, std::memory_order_relaxed);

    {WITH_LOCK(mtx) } // Acquire and release mutex to synchronize shadow write with sweepers.

    // Spin until no sweepers that haven't seen our update can sweep the unmapped region.
    // This prevents SIGSEGV in the sweeper.
    for (int checked_to = 0; checked_to < minesweeper::NUM_SWEEPERS+1;) {
        void *sb = sweepBlocks[checked_to].x.load(std::memory_order_acquire);
        if (!intersect(sb, SweepBlockSize, addr, size))
            checked_to++;
        else
            pthread_yield();
    }
}

void MemoryRanges::remapRange(void *addr, size_t size) {
    unmappedPages.clear(addr, add_bytes(addr, size));
    unmappedMem.fetch_sub(size, std::memory_order_relaxed);

    {WITH_LOCK(mtx) /* NOP -- flush changes */ }
}

void MemoryRanges::apply(apply_fn fn, int i) {
    for (size_t i = 0; i < mapRangeCount; i+=minesweeper::NUM_SWEEPERS+1)
        fn(mapRangeStarts[i], mapRangeSizes[i]);

    if (!minesweeper::STOP_WORLD_RESWEEP) {
        // Stack always assumed dirty, only scan here if we're not re-scanning
        for (size_t i = 0; i < stackCount; i++) {
            if (!stacks[i]) continue; // Thread exited, skip
            fn(stacks[i]->stackTop,
               size_bytes(stacks[i]->stackTop, stacks[i]->stackBottom));
        }
    }
}

void MemoryRanges::apply(apply_fn fn, int i, void *start, void *end) {
    ASSERT(start == page_aligned(start));
    ASSERT(end == page_aligned(end));

    for (void *block = start; block < end; block = add_bytes(block, SweepBlockSize)) {
        {WITH_LOCK(mtx) sweepBlocks[i].x.store(block, std::memory_order_relaxed); }

        size_t block_size = SweepBlockSize;
        if (size_bytes(block, end) < block_size)
            block_size = size_bytes(block, end);

        if (!unmappedPages.marked(block, block_size)) {
            // Fast path -- whole block mapped; perform single call
            fn(block, block_size);
        } else {
            // Slow path -- some (or all) pages in the block are unmapped
            // Proceed page-by-page
            for (void *page = block;
                 page < add_bytes(block, SweepBlockSize);
                 page = add_bytes(page, PAGE_SIZE)) {
                if (!isUnmapped(page))
                    fn(page, PAGE_SIZE);
            }
        }
    }

    sweepBlocks[i].x.store(NULL, std::memory_order_release);

    for (size_t j = i; j < mapRangeCount; j+=minesweeper::NUM_SWEEPERS+1)
        fn(mapRangeStarts[j], mapRangeSizes[j]);
}


void MemoryRanges::applyDirtyNoLock(apply_fn fn, void *start, void *end) {
    ASSERT(start == page_aligned(start));
    ASSERT(end == page_aligned(end));

    for (void *page = start; page < end; page = add_bytes(page, PAGE_SIZE)) {
        if (!unmappedPages.marked(page)
                && stop_the_world().isPageDirty((uintptr_t)page))
            fn(page, PAGE_SIZE);
    }
}

void MemoryRanges::applyDirtyNoLock(apply_fn fn, int id) {
    for (size_t i = id; i < mapRangeCount; i+=minesweeper::NUM_SWEEPERS+1)
        applyDirtyNoLock(fn, mapRangeStarts[i],
                         add_bytes(mapRangeStarts[i],mapRangeSizes[i]));

    if (id) return;

    for (size_t i = 0; i < stackCount; i++) {
        if (!stacks[i]) continue; // Thread exited, skip
        // Always assume stack dirty
        fn(stacks[i]->stackTop,
           size_bytes(stacks[i]->stackTop, stacks[i]->stackBottom));
    }
}

