#ifndef __HELPER_DEFS_H_INCLUDED
#define __HELPER_DEFS_H_INCLUDED

#include <mutex>
#include <csignal>
#include "stddef.h"
#include "stdint.h"
#include <string.h>
#include <unistd.h>

using lock_t = std::unique_lock<std::mutex>;
#define CONCAT_IMPL( x, y ) x##y
#define MACRO_CONCAT( x, y ) CONCAT_IMPL( x, y )
#define WITH_LOCK(mtx) lock_t (MACRO_CONCAT(with_lock_, __COUNTER__))(mtx);

#define NOINLINE __attribute__((noinline))

inline void msweeper_log(const char* m) {
  write(STDERR_FILENO, (const void*)m, strlen(m));
  fsync(STDERR_FILENO);
}

inline void msweeper_log_val(const char* m, uint64_t val) {
  // print m
  write(STDERR_FILENO, (const void*)m, strlen(m));

  // convert val into string (without risking using malloc/free)
  char str[30]; str[29] = '\0'; str[28] = '\n';
  int i;
  for (i = 0; val; val /= 10, i++) {
    if (i % 4 == 3) {
        str[27-i] = '\'';
        i++;
    }
    str[27-i] = (val % 10) + '0';
  }
  for (;i<=27;i++) str[27-i] = ' ';

  // print val
  write(STDERR_FILENO, (const void*)str, 29);
  // sync
  fsync(STDERR_FILENO);
}


#ifdef DEBUG
  #define S1(X) #X
  #define S2(X) S1(X)
  #define STR_LINE S2(__LINE__)
  #define ASSERT_MSG(b, str) do { if (!(b)) { msweeper_log((str)); std::raise(SIGABRT); } } while (0);
  #define ASSERT(b) ASSERT_MSG(b, ("Assertion \'" #b "\' failed at " __FILE__ ":" STR_LINE ".\n"))

  inline void msweeper_dlog(const char* m) { msweeper_log(m); }
  inline void msweeper_dlog_val(const char* m, uint64_t val) { msweeper_log_val(m, val); }
#else
  #define ASSERT_MSG(b, str) (b)
  #define ASSERT(b) (b)
  inline void msweeper_dlog(const char* m) {}
  inline void msweeper_dlog_val(const char* m, uint64_t val) {}
#endif

inline void *target(void *ptr) { return *((void**)ptr); }

inline void *add_words(void* ptr, size_t diff)
{ return (void*) (((void**)ptr) + diff); }
inline void *add_bytes(void* ptr, size_t diff)
{ return (void*) (((char*)ptr) + diff); }

#ifdef __cplusplus
inline void *inc_words(void*& ptr)
{ return ptr = add_words(ptr, 1); }
inline void *inc_bytes(void*& ptr) 
{ return ptr = add_bytes(ptr, 1); }
#endif

inline size_t size_bytes(void *start, void *end)
{ return (size_t)(((uintptr_t)end) - ((uintptr_t)start)); }
inline size_t size_words(void *start, void *end)
{ return (size_t)(((void**)end) - ((void**)start)); }

inline bool intersect(void *p1, size_t s1, void *p2, size_t s2)
{ return (add_bytes(p1,s1) > p2) && (add_bytes(p2,s2) > p1); }

static inline void* aligned_down(void *ptr, size_t alignment) {
    uintptr_t mask = ~(alignment - 1);

    uintptr_t p = (uintptr_t) ptr;
    return (void*) (p & mask);
}

static inline void* aligned_up(void *ptr, size_t alignment) {
    void *a_ptr = aligned_down(ptr, alignment);

    if ((uintptr_t)a_ptr < (uintptr_t)ptr)
        return add_bytes(a_ptr, alignment);
    return a_ptr;
}

// Is x a power of 2?
inline constexpr bool isPo2(size_t x) { return !(x & (x-1)); }

#define PAGE_SIZE 4096
static inline void* page_aligned(void *ptr) { return aligned_down(ptr, PAGE_SIZE); }
static inline void* page_aligned_up(void *ptr) { return aligned_up(ptr, PAGE_SIZE); }

static inline void *min(void *x, void *y) {
    return ((uintptr_t)x) < ((uintptr_t)y) ? x : y;
}

static inline void *max(void *x, void *y) {
    return ((uintptr_t)x) < ((uintptr_t)y) ? y : x;
}

#define MIN(x,y) (((x) < (y)) ? (x) : (y))

#define CMP(x,y) do { if ((x)<(y)) return -1; if ((y)<(x)) return 1; } while (0)

// Statistics atomic update (can always be relaxed)
#define STAT_MEM_ORD std::memory_order_relaxed
#define ST_STORE(x) store((x), STAT_MEM_ORD)
#define ST_LOAD load(STAT_MEM_ORD)
#define ST_ADD(x) fetch_add((x), STAT_MEM_ORD)
#define ST_SUB(x) fetch_sub((x), STAT_MEM_ORD)

#endif // __HELPER_DEFS_H_INCLUDED

